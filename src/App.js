import React, { Component } from 'react'
import Table from './Table'
import Form from './Form'

class App extends Component {
  state = {
    characters: [{
      'name': 'George Castillo',
      'job': 'abcde'
    },{
      'name': 'Gabriela Zavala',
      'job': 'fghij'
    }]
  }

  removeCharacter = (idx) => {
    const { characters } = this.state;

    this.setState({
      characters: characters.filter((character, i) => {
        return i !== idx;
      })
    })

  }

  handleSubmit = character => {
    this.setState({characters: [...this.state.characters, character]});
  }

  render() {
    const { characters } = this.state;
    
    return (
      <div className="container">
        <h1>FIRST REACTION</h1>
        <p>bohemian sunshine</p>
        <Table characterData={characters} removeCharacter={this.removeCharacter}>
          <h3>Add New</h3>
        </Table>
        <Form handleSubmit={this.handleSubmit}/>
      </div>
    )
  }

}

export default App
